package com.example.holuong.myjobs.modules.myjobs.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneNumber {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("_refs")
    @Expose
    private Object refs;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phones")
    @Expose
    private List<Phone> phones = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getRefs() {
        return refs;
    }

    public void setRefs(Object refs) {
        this.refs = refs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

}