package com.example.holuong.myjobs;

/**
 * Created by HoLuong on 12/21/2017.
 */

public class Constant {

    public static final String PREF_COOKIES = "pref_cookies";
    public static final String PREF_NAME = "pref_name";
    public static final String SUCCESS = "success";
    public static final String KEY_USER_ID = "user_id";
}
