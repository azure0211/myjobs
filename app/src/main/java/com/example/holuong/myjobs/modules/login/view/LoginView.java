package com.example.holuong.myjobs.modules.login.view;

public interface LoginView {

    void onLoginSuccess(String userId);

    void onLoginError(String message);
}
