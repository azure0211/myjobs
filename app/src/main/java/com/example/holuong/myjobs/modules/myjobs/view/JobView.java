package com.example.holuong.myjobs.modules.myjobs.view;

import com.example.holuong.myjobs.modules.myjobs.model.response.Job;

import java.util.List;

public interface JobView {

    void onGetMyJobsSuccess(List<Job> jobList);

    void onGetMyJobsError(String message);
}
