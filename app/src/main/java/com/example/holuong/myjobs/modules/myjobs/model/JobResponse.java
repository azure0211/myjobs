package com.example.holuong.myjobs.modules.myjobs.model;

import com.example.holuong.myjobs.model.AdditionalData;
import com.example.holuong.myjobs.modules.myjobs.model.response.Job;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HoLuong on 12/20/2017.
 */

public class JobResponse {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("data")
    @Expose
    private List<Job> data = null;
    @SerializedName("additionalData")
    @Expose
    private Object additionalData;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Job> getData() {
        return data;
    }

    public void setData(List<Job> data) {
        this.data = data;
    }

    public Object getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(Object additionalData) {
        this.additionalData = additionalData;
    }
}
