package com.example.holuong.myjobs.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.example.holuong.myjobs.Constant;

public class ConfigManager {

    private SharedPreferences pref;

    public ConfigManager(Context context) {
        this.pref = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE);
    }

    public void putString(String key, String value) {
        Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key, String defaultValue) {
        return pref.getString(key, defaultValue);
    }

    public void remove(String key) {
        Editor editor = pref.edit();
        editor.remove(key);
        editor.commit();
    }
}
