package com.example.holuong.myjobs.modules.myjobs.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Job {

    @SerializedName("requestId")
    @Expose
    private String requestId;
    @SerializedName("taskDetailRequestId")
    @Expose
    private Object taskDetailRequestId;
    @SerializedName("clientName")
    @Expose
    private String clientName;
    @SerializedName("clientEmail")
    @Expose
    private Object clientEmail;
    @SerializedName("imageBase64")
    @Expose
    private Object imageBase64;
    @SerializedName("imageLogoWidth")
    @Expose
    private Integer imageLogoWidth;
    @SerializedName("imageLogoHeight")
    @Expose
    private Integer imageLogoHeight;
    @SerializedName("additionalEmails")
    @Expose
    private List<Object> additionalEmails = null;
    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("clientServiceAddress")
    @Expose
    private ClientServiceAddress clientServiceAddress;
    @SerializedName("startDate")
    @Expose
    private Long startDate;
    @SerializedName("endDate")
    @Expose
    private Long endDate;
    @SerializedName("userRegId")
    @Expose
    private String userRegId;
    @SerializedName("requestDate")
    @Expose
    private Long requestDate;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("phoneNumbers")
    @Expose
    private List<PhoneNumber> phoneNumbers = null;
    @SerializedName("secondaryPhoneNumbers")
    @Expose
    private List<Object> secondaryPhoneNumbers = null;
    @SerializedName("serviceRequest")
    @Expose
    private Object serviceRequest;
    @SerializedName("userTaskDetailsId")
    @Expose
    private String userTaskDetailsId;
    @SerializedName("fromSubclient")
    @Expose
    private Boolean fromSubclient;
    @SerializedName("requestStatus")
    @Expose
    private String requestStatus;
    @SerializedName("serviceRequestType")
    @Expose
    private String serviceRequestType;
    @SerializedName("contractPayments")
    @Expose
    private Object contractPayments;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Object getTaskDetailRequestId() {
        return taskDetailRequestId;
    }

    public void setTaskDetailRequestId(Object taskDetailRequestId) {
        this.taskDetailRequestId = taskDetailRequestId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Object getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(Object clientEmail) {
        this.clientEmail = clientEmail;
    }

    public Object getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(Object imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public Integer getImageLogoWidth() {
        return imageLogoWidth;
    }

    public void setImageLogoWidth(Integer imageLogoWidth) {
        this.imageLogoWidth = imageLogoWidth;
    }

    public Integer getImageLogoHeight() {
        return imageLogoHeight;
    }

    public void setImageLogoHeight(Integer imageLogoHeight) {
        this.imageLogoHeight = imageLogoHeight;
    }

    public List<Object> getAdditionalEmails() {
        return additionalEmails;
    }

    public void setAdditionalEmails(List<Object> additionalEmails) {
        this.additionalEmails = additionalEmails;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public ClientServiceAddress getClientServiceAddress() {
        return clientServiceAddress;
    }

    public void setClientServiceAddress(ClientServiceAddress clientServiceAddress) {
        this.clientServiceAddress = clientServiceAddress;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getUserRegId() {
        return userRegId;
    }

    public void setUserRegId(String userRegId) {
        this.userRegId = userRegId;
    }

    public Long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Long requestDate) {
        this.requestDate = requestDate;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Object> getSecondaryPhoneNumbers() {
        return secondaryPhoneNumbers;
    }

    public void setSecondaryPhoneNumbers(List<Object> secondaryPhoneNumbers) {
        this.secondaryPhoneNumbers = secondaryPhoneNumbers;
    }

    public Object getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(Object serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public String getUserTaskDetailsId() {
        return userTaskDetailsId;
    }

    public void setUserTaskDetailsId(String userTaskDetailsId) {
        this.userTaskDetailsId = userTaskDetailsId;
    }

    public Boolean getFromSubclient() {
        return fromSubclient;
    }

    public void setFromSubclient(Boolean fromSubclient) {
        this.fromSubclient = fromSubclient;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getServiceRequestType() {
        return serviceRequestType;
    }

    public void setServiceRequestType(String serviceRequestType) {
        this.serviceRequestType = serviceRequestType;
    }

    public Object getContractPayments() {
        return contractPayments;
    }

    public void setContractPayments(Object contractPayments) {
        this.contractPayments = contractPayments;
    }

}