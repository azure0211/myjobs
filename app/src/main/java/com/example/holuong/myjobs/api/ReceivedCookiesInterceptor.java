package com.example.holuong.myjobs.api;

import android.content.Context;

import com.example.holuong.myjobs.Constant;
import com.example.holuong.myjobs.config.ConfigManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by HoLuong on 12/21/2017.
 */

public class ReceivedCookiesInterceptor implements Interceptor {

    private Context mContext;

    public ReceivedCookiesInterceptor(Context context) {
        this.mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {

            for (String header : originalResponse.headers("Set-Cookie")) {
                if (header.contains("JSESSIONID")) {
                    // save cookie
                    new ConfigManager(mContext).putString(Constant.PREF_COOKIES, header);
                }
            }
        }

        return originalResponse;
    }
}
