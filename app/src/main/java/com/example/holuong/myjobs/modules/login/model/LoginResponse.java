package com.example.holuong.myjobs.modules.login.model;

import com.example.holuong.myjobs.model.AdditionalData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HoLuong on 12/20/2017.
 */

public class LoginResponse {

    @SerializedName("additionalData")
    private AdditionalData additionalData;
    @SerializedName("data")
    private LoginData data;
    @SerializedName("text")
    private String text;
    @SerializedName("type")
    private String type;

    public AdditionalData getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(AdditionalData additionalData) {
        this.additionalData = additionalData;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
