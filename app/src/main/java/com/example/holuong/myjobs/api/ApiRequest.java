package com.example.holuong.myjobs.api;

import com.example.holuong.myjobs.modules.login.model.LoginResponse;
import com.example.holuong.myjobs.modules.myjobs.model.JobResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.Observable;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HoLuong on 12/20/2017.
 */

public class ApiRequest {

    public static final String SERVER_URL = "http://52.74.220.125:10005/ecom-services/api/";

    private ApiService apiService;

    public ApiRequest() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit = builder
                .client(new OkHttpClient())
                .baseUrl(SERVER_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    public ApiRequest(Interceptor interceptor) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.addInterceptor(interceptor);
        OkHttpClient client = clientBuilder.build();

        Retrofit.Builder builder = new Retrofit.Builder();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = builder
                .client(client)
                .baseUrl(SERVER_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    public Observable<LoginResponse> login(String email, String password) {
        return apiService.login(email, password);
    }

    public Observable<JobResponse> getMyJobs(String sessionId, String userId) {
        return apiService.getMyJobs(sessionId, userId);
    }
}
