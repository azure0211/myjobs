package com.example.holuong.myjobs.modules.login.presenter;


import android.content.Context;

import com.example.holuong.myjobs.Constant;
import com.example.holuong.myjobs.api.ApiRequest;
import com.example.holuong.myjobs.api.ReceivedCookiesInterceptor;
import com.example.holuong.myjobs.modules.login.model.LoginData;
import com.example.holuong.myjobs.modules.login.view.LoginView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter {

    private LoginView mLoginView;
    private Context mContext;

    public LoginPresenter(Context context, LoginView view) {
        this.mContext = context;
        this.mLoginView = view;
    }

    public void login(String email, String password) {
        new ApiRequest(new ReceivedCookiesInterceptor(mContext)).login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginResponse -> {
                    if (loginResponse.getType().equals(Constant.SUCCESS)) {
                        LoginData data = loginResponse.getData();
                        mLoginView.onLoginSuccess(data.userId);
                    } else
                        mLoginView.onLoginError(loginResponse.getText());
                }, throwable -> mLoginView.onLoginError(throwable.toString()));
    }
}
