package com.example.holuong.myjobs.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.holuong.myjobs.R;
import com.example.holuong.myjobs.modules.myjobs.model.response.Job;

import java.util.Date;
import java.util.List;

/**
 * Created by HoLuong on 12/21/2017.
 */

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.JobViewHolder> {

    List<Job> jobs;

    public JobAdapter(List<Job> jobs) {
        this.jobs = jobs;
    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_job, viewGroup, false);
        JobViewHolder viewHolder = new JobViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(JobViewHolder jobViewHolder, int i) {
        Job job = jobs.get(i);
        jobViewHolder.tvDate.setText(parseDateTime(job.getStartDate()) + " - " + parseDateTime(job.getEndDate()));
        jobViewHolder.tvServiceName.setText(job.getServiceName());
        jobViewHolder.tvClientName.setText(job.getClientName());
        jobViewHolder.tvRequestStatus.setText(job.getRequestStatus());
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public class JobViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate;
        TextView tvServiceName;
        TextView tvClientName;
        TextView tvRequestStatus;

        JobViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvClientName = itemView.findViewById(R.id.tvClientName);
            tvRequestStatus = itemView.findViewById(R.id.tvRequestStatus);
        }
    }

    private String parseDateTime(long dateTime) {
        Date date = new Date(dateTime);
        return date.getHours() + ":" + date.getMinutes();
    }
}