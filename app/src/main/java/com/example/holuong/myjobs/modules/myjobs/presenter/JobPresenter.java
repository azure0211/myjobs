package com.example.holuong.myjobs.modules.myjobs.presenter;


import android.content.Context;

import com.example.holuong.myjobs.Constant;
import com.example.holuong.myjobs.api.ApiRequest;
import com.example.holuong.myjobs.config.ConfigManager;
import com.example.holuong.myjobs.modules.myjobs.view.JobView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class JobPresenter {

    JobView mJobView;
    Context mContext;
    ApiRequest mApiRequest;

    public JobPresenter(Context context, JobView view, ApiRequest apiRequest) {
        this.mContext = context;
        this.mJobView = view;
        this.mApiRequest = apiRequest;
    }

    public void getMyJobs(String userId) {
        String sessionId = new ConfigManager(mContext).getString(Constant.PREF_COOKIES, null);
        mApiRequest.getMyJobs(sessionId, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(jobResponse -> {
                    if (jobResponse.getType().equals(Constant.SUCCESS)) {
                        mJobView.onGetMyJobsSuccess(jobResponse.getData());
                    } else
                        mJobView.onGetMyJobsError(jobResponse.getText());
                }, throwable -> mJobView.onGetMyJobsError(throwable.toString()));
    }
}
