package com.example.holuong.myjobs.api;

import com.example.holuong.myjobs.modules.login.model.LoginResponse;
import com.example.holuong.myjobs.modules.myjobs.model.JobResponse;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by HoLuong on 12/20/2017.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("authenticate")
    public Observable<LoginResponse> login(@Field("username") String username,
                                           @Field("password") String password);

    @GET("mobile/tasks/user/{id}/upcoming")
    public Observable<JobResponse> getMyJobs(@Header("Cookie") String sessionId, @Path("id") String userId);
}
