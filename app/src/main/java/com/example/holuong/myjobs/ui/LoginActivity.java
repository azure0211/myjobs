package com.example.holuong.myjobs.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.holuong.myjobs.R;
import com.example.holuong.myjobs.modules.login.presenter.LoginPresenter;
import com.example.holuong.myjobs.modules.login.view.LoginView;

import static com.example.holuong.myjobs.Constant.KEY_USER_ID;

public class LoginActivity extends BaseActivity implements LoginView {

    LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPresenter = new LoginPresenter(this, this);
    }

    @Override
    public void onLoginSuccess(String userId) {
        dismissProgressDialog();
        Intent intent = new Intent(this, MyJobsActivity.class);
        intent.putExtra(KEY_USER_ID, userId);
        startActivity(intent);
    }

    @Override
    public void onLoginError(String message) {
        dismissProgressDialog();
        showToast(message);
    }

    public void signIn(View view) {
        EditText etEmail = findViewById(R.id.etEmail);
        EditText etPassword = findViewById(R.id.etPassword);

        showProgressDialog();
        mPresenter.login(etEmail.getText().toString(), etPassword.getText().toString());
    }
}
