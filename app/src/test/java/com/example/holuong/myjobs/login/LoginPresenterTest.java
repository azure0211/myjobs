package com.example.holuong.myjobs.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.holuong.myjobs.RxImmediateSchedulerRule;
import com.example.holuong.myjobs.modules.login.presenter.LoginPresenter;
import com.example.holuong.myjobs.modules.login.view.LoginView;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Created by HoLuong on 12/22/2017.
 */

public class LoginPresenterTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    @Mock
    LoginView loginView;
    @Mock
    Context context;
    @Mock
    SharedPreferences sharedPrefs;
    @Mock
    SharedPreferences.Editor editor;
    @InjectMocks
    LoginPresenter loginPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loginSuccessful() {
        String email = "diasl@outlook.com";
        String password = "password";

        Mockito.when(context.getSharedPreferences(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(sharedPrefs);
        Mockito.when(sharedPrefs.edit())
                .thenReturn(editor);
        Mockito.when(editor.putString(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(editor);

        loginPresenter.login(email, password);

        Mockito.verify(loginView).onLoginSuccess(Mockito.anyString());
    }

    @Test
    public void loginFailed() {
        String email = "email";
        String password = "password";

        Mockito.when(context.getSharedPreferences(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(sharedPrefs);
        Mockito.when(sharedPrefs.edit())
                .thenReturn(editor);
        Mockito.when(editor.putString(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(editor);

        loginPresenter.login(email, password);

        Mockito.verify(loginView).onLoginError(Mockito.anyString());
    }
}
