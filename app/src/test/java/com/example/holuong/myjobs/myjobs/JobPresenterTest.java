package com.example.holuong.myjobs.myjobs;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.holuong.myjobs.Constant;
import com.example.holuong.myjobs.RxImmediateSchedulerRule;
import com.example.holuong.myjobs.api.ApiRequest;
import com.example.holuong.myjobs.modules.myjobs.model.JobResponse;
import com.example.holuong.myjobs.modules.myjobs.presenter.JobPresenter;
import com.example.holuong.myjobs.modules.myjobs.view.JobView;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by HoLuong on 12/22/2017.
 */

public class JobPresenterTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    @Mock
    JobView jobView;
    @Mock
    ApiRequest apiRequest;
    @Mock
    Context context;
    @Mock
    SharedPreferences sharedPrefs;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getMyJobsSuccessful() {
        String userId = "5891d7521d58301538b578fd";

        // job response
        JobResponse jobResponse = new JobResponse();
        jobResponse.setType(Constant.SUCCESS);
        jobResponse.setData(new ArrayList<>());
        Observable<JobResponse> observable = Observable.just(jobResponse);

        Mockito.when(context.getSharedPreferences(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(sharedPrefs);

        // sessionId = getString(Constant.PREF_COOKIES, null) = null
        Mockito.when(apiRequest.getMyJobs(Mockito.eq(null), Mockito.anyString()))
                .thenReturn(observable);

        JobPresenter jobPresenter = new JobPresenter(context, jobView, apiRequest);
        jobPresenter.getMyJobs(userId);

        Mockito.verify(jobView).onGetMyJobsSuccess(Mockito.anyList());
    }

    @Test
    public void getMyJobsError() {
        String userId = "5891d7521d58301538b578fd";

        // job response
        Exception exception = new Exception();
        Observable<JobResponse> observable = Observable.error(exception);

        Mockito.when(context.getSharedPreferences(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(sharedPrefs);

        // sessionId = getString(Constant.PREF_COOKIES, null) = null
        Mockito.when(apiRequest.getMyJobs(Mockito.eq(null), Mockito.anyString()))
                .thenReturn(observable);

        JobPresenter jobPresenter = new JobPresenter(context, jobView, apiRequest);
        jobPresenter.getMyJobs(userId);

        Mockito.verify(jobView).onGetMyJobsError(Mockito.anyString());
    }
}
